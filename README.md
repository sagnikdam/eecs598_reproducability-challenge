#########################################################################
This is a class project meant for the ICLR reproducibility challenge 2018
#########################################################################

For the implementation of the modified Q-Learning algorithms we have provided the following files :

  * standard.py
  * time_awareness.py
  * partial_bootstrap.py

The environment that we modelled on which the algorithm runs on is :
  * gridworld.py

We have modified the time_limit.py folder which originally exists in  /home/sagnik/.local/lib/python3.5/site-packages/gym/wrappers

You will have to replace the existing one with the modified ones that we have provided here according to whether you want to run the time-aware (time_limit_aware) or the partial bootstrapping versions(time_limit_partial).

The files which we have modified reside inside ./baselines/ppo1

The files that we have changed are :

  * run_mujoco.py
  * pposgd_simple.py
  * pposgd_simple1.py

We have also changed the ./baselines/common/monitor.py file

Currently it is configured to run the time-aware version. If you want to run the partial bootstrapping version, then you would have to replace these files with the files given outside into this folder.

  * run_mujoco_partial.py
  * pposgd_simple_partial.py
  * pposgd_simple1_partial.py
