import numpy as np
import gym
from gridworld import GridworldEnv
from colorama import init
from colorama import Fore, Back, Style

init(autoreset=False)

env = GridworldEnv()
env.reset()
env.render()
np.random.seed(42)
env.seed(0)

#Initializing Q-table
nS = env.observation_space.n
nA = env.action_space.n
Q_table = np.zeros((nS, nA))

#Initializing parameters
gamma = 0.99
alpha = 0.5
num_episodes = 5000
flag = 0
count = 0

#For all episodes
while(flag != 1):
	state = env.reset()
	s = state
	while (s==2 or s==10 or s==13 or s==14 or s==20 or s==21 or s==22 or s==24 or s==28 or s==5 or s==30):
		state = env.reset()
		s = state

	alpha = alpha - 0.0005
	if alpha < 0:
		alpha = 0

	#For each step of episode
	Time = 0
	while(Time <= 3):
		#Choosing a random action
		action = env.action_space.sample()
		new_state, reward, done, _ = env.step(action)

		#Updating Q table
		max_value = np.max(Q_table[new_state,:])
		Q_table[state,action] += alpha*(reward + gamma*max_value - Q_table[state,action])
		
		state = new_state

		if done == True:
			break

		Time += 1
	
	count += 1
	if(count==num_episodes):
		flag = 1

#Displaying optimal policy
optimal_policy = np.zeros((nS,nA))
for s in range(nS):
    optimal_policy[s][np.argmax(Q_table[s])] = 1.0

up = '\u25b2'
down = '\u25bc'
right = '\u25b6'
left = '\u25c0'
stay = '\u23fa'

char_array = [up, right, down, left, stay]
holes = [2,10,13,14,20,21,22,24,28]

print(Back.WHITE + '\n\n')

for i in range(6):
    for j in range(6):
    	if((i*6+j) in holes):

    		if (j == 0):
    			print('     ', end = '  ')
    		else:
    			print('  ', end = '  ')

    		
    	else:
    		action_select = np.argmax(Q_table[i*6 + j])
    		if Q_table[i*6+j,action_select] == Q_table[i*6+j,4]:
    			action_select = 4
    		if (j == 0):
    			print(Fore.BLUE + '  ',char_array[action_select], end = '   ')
    		else:
    			print(Fore.BLUE + char_array[action_select], end = '   ')

    		
    print('\n\n')

print(Style.RESET_ALL)












