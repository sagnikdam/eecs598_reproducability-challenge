#!/usr/bin/env python
from baselines.common import set_global_seeds, tf_util as U
from baselines import bench
import gym, logging
from baselines import logger
from gym.wrappers import TimeLimit
from gym.envs.mujoco import hopper 
import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np

def train(env_id, num_timesteps, seed):
    from baselines.ppo1 import mlp_policy, pposgd_simple
    U.make_session(num_cpu=1).__enter__()
    set_global_seeds(seed)
    env = gym.make(env_id).unwrapped
    def policy_fn(name, ob_space, ac_space):
        return mlp_policy.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
            hid_size=64, num_hid_layers=2)
    env = TimeLimit(env,max_episode_steps=300)
    env = bench.Monitor(env, logger.get_dir())
    env.seed(seed)

    gym.logger.setLevel(logging.WARN)
    ppo_rets = pposgd_simple.learn(env, policy_fn,
            max_timesteps=num_timesteps,
            timesteps_per_actorbatch=2048,
            clip_param=0.2, entcoeff=0.0,
            optim_epochs=10, optim_stepsize=3e-4, optim_batchsize=64,
            gamma=0.99, lam=0.95, schedule='linear',
        )
    env.close()
    U.get_session().close()
    return ppo_rets

def train1(env_id, num_timesteps, seed):
    from baselines.ppo1 import mlp_policy1, pposgd_simple1
    U.make_session(num_cpu=1).__enter__()
    set_global_seeds(seed)
    env = gym.make(env_id).unwrapped
    def policy_fn(name, ob_space, ac_space):
        return mlp_policy1.MlpPolicy(name=name, ob_space=ob_space, ac_space=ac_space,
            hid_size=64, num_hid_layers=2)
    env = TimeLimit(env,max_episode_steps=300)
    env = bench.Monitor(env, logger.get_dir())
    env.seed(seed)

    gym.logger.setLevel(logging.WARN)
    ppo_rets = pposgd_simple1.learn(env, policy_fn,
            max_timesteps=num_timesteps,
            timesteps_per_actorbatch=2048,
            clip_param=0.2, entcoeff=0.0,
            optim_epochs=10, optim_stepsize=3e-4, optim_batchsize=64,
            gamma=0.99, lam=0.95, schedule='linear',
        )
    env.close()
    U.get_session().close()
    return ppo_rets

def main():
    import argparse
    num_timesteps = 1e6
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--env', help='environment ID', default='Hopper-v1')
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--num-timesteps', type=int, default=int(num_timesteps))
    args = parser.parse_args()
    logger.configure()

    number_of_seeds = 5
    sliding_win_len = 10 
    len_vector = int(np.ceil(num_timesteps/2048))

    ppo_rets = [[] for i in range(number_of_seeds)]
    ppo_rets_avg = np.zeros(len_vector, 'float32')
    ppo_rets1 = [[] for i in range(number_of_seeds)]
    ppo_rets1_avg = np.zeros(len_vector, 'float32')

    for i in range(number_of_seeds):
      a = str(i)
      with tf.variable_scope(a):
        print("*********************Without awareness*********************** ",a,"\n")
        ppo_rets[i] = train(args.env, num_timesteps=args.num_timesteps, seed=i)
      ppo_rets_avg = [sum(x) for x in zip(ppo_rets_avg,ppo_rets[i])]

    for i in range(number_of_seeds,2*number_of_seeds):
      a = str(i)
      with tf.variable_scope(a):
        print("*********************With awareness*********************** ",a,"\n")
        ppo_rets1[i-number_of_seeds] = train1(args.env, num_timesteps=args.num_timesteps, seed=(i-number_of_seeds))
      ppo_rets1_avg = [sum(x) for x in zip(ppo_rets1_avg,ppo_rets1[i-number_of_seeds])]


    ppo_rets_avg[:] = [x / number_of_seeds for x in ppo_rets_avg]
    ppo_rets1_avg[:] = [x / number_of_seeds for x in ppo_rets1_avg]

    ## Finding upper and lower sigma bounds
    ppo_rets_std = np.zeros(len_vector, 'float32')
    ppo_rets_lower = np.zeros(len_vector, 'float32')
    ppo_rets_upper = np.zeros(len_vector, 'float32')

    ppo_rets1_std = np.zeros(len_vector, 'float32')
    ppo_rets1_lower = np.zeros(len_vector, 'float32')
    ppo_rets1_upper = np.zeros(len_vector, 'float32')

    temp = np.zeros(number_of_seeds, 'float32')
    temp1 = np.zeros(number_of_seeds, 'float32')

    for j in range(len_vector):
        for s in range(number_of_seeds):
            temp[s] = ppo_rets[s][j]
            temp1[s] = ppo_rets1[s][j]
        ppo_rets_std[j] = np.std(temp)
        ppo_rets1_std[j] = np.std(temp1)

  
    sliding_rets = []
    sliding_rets1 = []
    
    sliding_rets = np.convolve(ppo_rets_avg,np.ones((sliding_win_len,))/sliding_win_len,mode='same')
    sliding_rets1 = np.convolve(ppo_rets1_avg,np.ones((sliding_win_len,))/sliding_win_len,mode='same')

    ppo_rets_lower = [a_i - b_i for a_i,b_i in zip(sliding_rets,ppo_rets_std)]
    ppo_rets_upper = [sum(x) for x in zip(sliding_rets, ppo_rets_std)]

    ppo_rets1_lower = [a_i - b_i for a_i,b_i in zip(sliding_rets1,ppo_rets1_std)]
    ppo_rets1_upper = [sum(x) for x in zip(sliding_rets1, ppo_rets1_std)]

    x = list(range(1,len_vector+1))
    plt.plot(x[sliding_win_len:-sliding_win_len],sliding_rets[sliding_win_len:-sliding_win_len],c='#CC4F1B',label='PPO')
    plt.fill_between(x[sliding_win_len:-sliding_win_len],ppo_rets_lower[sliding_win_len:-sliding_win_len], ppo_rets_upper[sliding_win_len:-sliding_win_len],alpha=0.5, edgecolor='#CC4F1B', facecolor='#FF9848')

    plt.plot(x[sliding_win_len:-sliding_win_len],sliding_rets1[sliding_win_len:-sliding_win_len],c='#1B2ACC',label='PPO time')
    plt.fill_between(x[sliding_win_len:-sliding_win_len],ppo_rets1_lower[sliding_win_len:-sliding_win_len], ppo_rets1_upper[sliding_win_len:-sliding_win_len],alpha=0.5, edgecolor='#1B2ACC', facecolor='#089FFF')
    
    plt.ylabel('Estimated return per episode (Y=0.99)',fontsize=20)
    plt.xlabel('training timesteps',fontsize=20)
    plt.title('Hopper-v1 (T = 300)',fontsize=20)
    plt.legend()
    plt.show()

if __name__ == '__main__':
    main()
